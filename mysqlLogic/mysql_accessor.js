/**
 * Created by denko on 4/2/2016.
 */

console.log(":D");
var moment = require('moment')
var mysql = require('mysql'),
    fs = require('fs'),
    path = require('path'),
    config = JSON.parse(fs.readFileSync(path.join(__dirname, 'mysqlConfig.json'), "utf8")),
    configuration = JSON.parse(fs.readFileSync(path.join(__dirname, '../configuration.json'), "utf8"));
var dbConnection;
function handleDisconnect() {
    dbConnection = mysql.createConnection(config); // Recreate the connection, since
                                                    // the old one cannot be reused.

    dbConnection.connect(function(err) {              // The server is either down
        if(err) {                                     // or restarting (takes a while sometimes).
            console.log('error when connecting to db:', err);
            setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
        }                                     // to avoid a hot loop, and to allow our node script to
    });                                     // process asynchronous requests in the meantime.
                                            // If you're also serving http, display a 503 error.
    dbConnection.on('error', function(err) {
        console.log('db error', err);
        if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
            handleDisconnect();                         // lost due to either server restart, or a
        } else {                                      // connnection idle timeout (the wait_timeout
            throw err;                                  // server variable configures this)
        }
    });
}
handleDisconnect();
dbConnection.on('error', function(err) {
    console.log(err, "Err mysql connection");
});
var getValuta = require('../mssqlLogic/mssql_accessor').getValuta;

var checkDatabase = function (cb) {
    dbConnection.query(`SELECT * FROM ${configuration.TargetTable} LIMIT 100`, (err, rows) => {
        if (err) console.log("Err", JSON.stringify(err));
        cb(rows)
    });
};
checkDatabase((items) => {
    console.log('ok');
});

var insertUpdatePrices = function (rows, cb) {
    var updated = 0, created = 0;
    /*getValuta(valuta => {*/
        try {
            dbConnection.query(`SELECT * FROM ${configuration.PricesTable}`, (err, products) => {
                if (err) console.log("Err", JSON.stringify(err));
                rows.forEach(row => {
                    var found = false;
                    if (products)
                        for (var i = 0; i < products.length; i++) {
                            if (row.ProizvodSifra == products[i].magacin_sifra) {
                                found = true;
                               /* if (row.ProdaznaCena / valuta != products[i].cena_eur)
                                    dbConnection.query(`UPDATE ${configuration.PricesTable} SET cena_eur = ? WHERE magacin_sifra = ?`, [row.ProdaznaCena, row.ProizvodSifra], (err, rows) => {
                                        if (err) console.log("Err", JSON.stringify(err));
                                    });*/
                                break;
                            }
                        }
                    if (!found) {
                      /*  dbConnection.query(`INSERT INTO ${configuration.PricesTable} (magacin_sifra, cena_eur)
		VALUES ('${row.ProizvodSifra}', ${row.ProdaznaCena / valuta})`, (err, rows) => {
                            if (err) console.log("Err", JSON.stringify(err));
                        });*/
                          dbConnection.query(`INSERT INTO ${configuration.PricesTable} (magacin_sifra)
                         VALUES ('${row.ProizvodSifra}')`, (err, rows) => {
                         if (err) console.log("Err", JSON.stringify(err));
                         });
                          created++
                    }
                })
                cb({created, updated});
            });
        } catch (ex) {
            console.log("Error", ex);
        }
   /* })*/



    // products.forEach(product => {
    //     dbConnection.query(`INSERT INTO ${configuration.TargetTable} (magacin_sifra, magacin_ime, promeneto_utc)
    // VALUES ('${product.ProizvodSifra}', '${product.ProizvodIme}', '${moment().format('YYYY-MM-DD HH:mm:ss')}')
    // ON DUPLICATE KEY UPDATE magacin_ime = IF(magacin_ime <> '${product.ProizvodIme}','${product.ProizvodIme}', magacin_ime), promeneto_utc = IF(magacin_ime <> '${product.ProizvodIme}','${moment().format('YYYY-MM-DD HH:mm:ss')}', promeneto_utc)`, (err, rows) => {
    //         if (err) throw err;
    //     })
    // });

};



var insertUpdateProducts = function (rows, cb) {
    var updated = 0;
    var created = 0;
    try {
        dbConnection.query(`SELECT * FROM ${configuration.TargetTable}`, (err, products) => {
            if (err) console.log("Err", JSON.stringify(err));
            rows.forEach(row => {
                var found = false;
                if (products)
                    for (var i = 0; i < products.length; i++) {
                        if (row.ProizvodSifra == products[i].magacin_sifra) {
                            found = true;
                            if (row.ProizvodIme != products[i].magacin_ime) {
                                dbConnection.query(`UPDATE ${configuration.TargetTable} SET magacin_ime = ?, promeneto_utc = ? WHERE magacin_sifra = ?`, [row.ProizvodIme, moment.utc().format('YYYY-MM-DD HH:mm:ss'), row.ProizvodSifra], (err, rows) => {
                                    if (err) console.log("Err", JSON.stringify(err));
                                });
                                updated++;
                            }
                            break;
                        }
                    }
                if (!found) {
                    dbConnection.query(`INSERT INTO ${configuration.TargetTable} (magacin_sifra, magacin_ime, promeneto_utc)
		VALUES ('${row.ProizvodSifra}', '${row.ProizvodIme}', '${moment.utc().format('YYYY-MM-DD HH:mm:ss')})')`, (err, rows) => {
                        if (err) console.log("Err", JSON.stringify(err));
                    });
                    created++;
                }

            })
            cb({created, updated});
        });
    } catch (ex) {
        console.log("Error", ex);
    }


    // products.forEach(product => {
    //     dbConnection.query(`INSERT INTO ${configuration.TargetTable} (magacin_sifra, magacin_ime, promeneto_utc)
    // VALUES ('${product.ProizvodSifra}', '${product.ProizvodIme}', '${moment().format('YYYY-MM-DD HH:mm:ss')}')
    // ON DUPLICATE KEY UPDATE magacin_ime = IF(magacin_ime <> '${product.ProizvodIme}','${product.ProizvodIme}', magacin_ime), promeneto_utc = IF(magacin_ime <> '${product.ProizvodIme}','${moment().format('YYYY-MM-DD HH:mm:ss')}', promeneto_utc)`, (err, rows) => {
    //         if (err) throw err;
    //     })
    // });

};

var insertUpdateMagacin = (magacinTableName, rows, cb) => {
    var updated = 0;
    var created = 0;
    try {
        dbConnection.query(`SELECT * FROM ${magacinTableName}`, (err, products) => {
            if (err) console.log("Err", JSON.stringify(err));
            rows.forEach(row => {
                var found = false;
                if (products)
                    for (var i = 0; i < products.length; i++) {
                        if (row.ProizvodSifra == products[i].magacin_sifra) {
                            found = true;
                            if (row.Kolicina != products[i].magacin_kolicina || row.ProdaznaCena != products[i].magacin_cena) {
                                dbConnection.query(`UPDATE ${magacinTableName} SET magacin_cena = ${row.ProdaznaCena}, magacin_kolicina = ${row.Kolicina}, promeneto_utc = '${moment.utc().format('YYYY-MM-DD HH:mm:ss')}' WHERE magacin_sifra = '${row.ProizvodSifra}'`, (err, rows) => {
                                    if (err) console.log("Err", JSON.stringify(err));
                                });
                                updated++;
                            }
                        }
                    }
                if (!found) {
                    dbConnection.query(`INSERT INTO ${magacinTableName} (magacin_sifra, magacin_cena, magacin_kolicina, promeneto_utc)
	VALUES ('${row.ProizvodSifra}' , ${row.ProdaznaCena}, ${row.Kolicina}, '${moment.utc().format('YYYY-MM-DD HH:mm:ss')}')`, (err, rows) => {
                        if (err) console.log("Err", JSON.stringify(err));
                    })
                    created++;
                }

            })
            cb({updated, created});
        });
    } catch (ex) {
        console.log("Error", ex);
    }


};
var truncateTables = (cb) => {
    var truncateQuery = `TRUNCATE ${configuration.TargetTable};`;
    configuration.Magacini.forEach(magacin => {
        truncateQuery += `TRUNCATE ${magacin.Ime};`
    });
    dbConnection.query(truncateQuery, data => {
        cb(true);
    });
};

//truncateTables();
exports.insertUpdateProducts = insertUpdateProducts;
exports.insertUpdateMagacin = insertUpdateMagacin;
exports.insertUpdatePrices = insertUpdatePrices;
exports.truncateTables = truncateTables;