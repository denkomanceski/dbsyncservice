var sql = require('mssql');
var fs = require('fs');
var path = require('path');
var config = JSON.parse(fs.readFileSync(path.join(__dirname, 'mssqlConfig.json'), "utf8"));
var configuration = JSON.parse(fs.readFileSync(path.join(__dirname, '../configuration.json'), "utf8"));
var queryServer = (query, cb) => {
    var connection1 = new sql.Connection(config, function (err) {
        var request = new sql.Request(connection1);
        request.query(query, (err, rows) => {
            if (err) console.log("Err", JSON.stringify(err));
            cb(rows);
            connection1.close();
        });

    });
};
var findLocalDatabase = (cb) => {
    try {
        queryServer(`SELECT ProizvodTipBase FROM tblKorisnici WHERE Korisnik = ${config.webUserId}`, users=> {
            if (users)
                if (users.length > 0) {
                    config.originalDatabase = config.database;
                    if (users[0].ProizvodTipBase == 1) {
                        config.localDatabase = `${config.database}_${config.webUserId}`
                    } else {
                        config.localDatabase = config.database;
                    }
                }
                else {
                    console.log("Korisnik so toa ID ne postoi");
                }
            console.log(`Lokalna baza: ${config.localDatabase}, ${config.originalDatabase}`);
            //console.log(JSON.stringify(config), "CONFIG");
            cb(config)
        })
    } catch (ex) {
        console.log("Error", ex);
    }

};

var getProducts = (cb) => {
    try {
        // queryServer(`SELECT ProizvodSifra, ProizvodIme FROM tblProizvod WHERE GrupaNaProizvod in (${configuration.GrupiNaProizvodi.join(',')})`, (proizvodi) => {
        queryServer(`SELECT ProizvodSifra, ProizvodIme FROM tblProizvod WHERE ZaOnline = 1`, (proizvodi) => {
            cb(proizvodi || [])
        })
    } catch (ex) {
        console.log("Error", ex);
    }

};
var getValuta = (cb) => {
    queryServer(`SELECT Kurs FROM tblValuta WHERE Valuta = 'EUR'`, (rows) => {/**/
        if (rows)
            if (rows.length > 0)
                cb(rows[0].Kurs)
            else
                throw "Error fetching EURO valuta"
    });
};

var getProductsFromMagacin = (magacini_IDs, cb) => {
    try {
        queryServer(`SELECT ProizvodSifra, MAX(ProdaznaCena) as ProdaznaCena, SUM(Kolicina) as Kolicina FROM [${config.originalDatabase}_${config.webUserId}].[dbo].[tblMagacinSintetika] a WHERE ProizvodSifra in (SELECT ProizvodSifra FROM tblProizvod WHERE GrupaNaProizvod in (${configuration.GrupiNaProizvodi.join(',')})) AND MagacinID in (${magacini_IDs}) GROUP BY a.ProizvodSifra`, proizvodi => {
            cb(proizvodi || [])
        })
    } catch (ex) {
        console.log("Error", ex);
    }

};
queryServer('SELECT TOP 100 * FROM tblProizvod', (data) => {
    if (data)
        if (data.length > 0)
            console.log("DATA");
});

exports.queryServer = queryServer;
exports.config = config;
exports.findLocalDatabase = findLocalDatabase;
exports.getProducts = getProducts;
exports.getProductsFromMagacin = getProductsFromMagacin;
exports.getValuta = getValuta;
