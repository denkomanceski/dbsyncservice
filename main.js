/**
 * Created by denkomanceski on 8/12/16.
 */
var mssql = require('./mssqlLogic/mssql_accessor');
var mysql = require('./mysqlLogic/mysql_accessor');
var fs = require('fs');
var path = require('path');
var configuration = JSON.parse(fs.readFileSync(path.join(__dirname, 'configuration.json'), "utf8"));
var interval;
var express = require('express');
var bodyParser = require('body-parser');
var http = require('http');
var debug = require('debug');
var app = express();
app.use(bodyParser({limit: '50mb'}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
var router = express.Router();
router.post('/api/product', (req, res) => {
    console.log(JSON.stringify(req.body), "body");
    console.log(JSON.stringify(req.query), "query");
    var ctr = 0;
    var data = [];
    if(req.body.key == 'c&SeVef8b4&-wu3Etupr2tr=+ac477+P') {
        sync((stats) => {
            ctr++;
            data.push(stats);
            if(ctr == 3){
                res.send({response: data})
            }
        });

    } else {
        res.send({response: "Err: 10"})
    }
});
router.get('/', (req, res) => {
    res.send({response: "Servisot e zapocnat..."})
});
app.use('/', router);
var server = http.createServer(app);
server.on('error', onError);
server.on('listening', onListening);
server.listen(3000);
mssql.findLocalDatabase(() => {
    console.log(`Syncing every ${configuration.Interval} minutes.`);
    mysql.truncateTables(data => {
        sync(data => {
            console.log(JSON.stringify(data), "data");
        })
    });

    interval = setInterval(() => {
        console.log("Syncing....");
        sync(stats => {
            console.log(JSON.stringify(stats), "stats");
        })
    }, configuration.Interval * 60 * 1000)
});
var sync = function (cb) {
    try {
        var magaciniIDs = [];
        configuration.Magacini.forEach(magacin => {
            console.log(`Syncing magacin ${magacin.IDs}`);
            magaciniIDs = magaciniIDs.concat(magacin.IDs);
            mssql.getProductsFromMagacin(magacin.IDs, (proizvodi) => {
                //MySQL Upsert
                if (proizvodi)
                    if (proizvodi.length > 0)
                        mysql.insertUpdateMagacin(magacin.Ime, proizvodi, success => {
                            console.log("Updated");
                            cb({'Magacini': success})
                        });
                    else
                        console.log("Proizvodi - fetch problem");
            })
        });
        mssql.getProductsFromMagacin(magaciniIDs, (proizvodi) => {
            if (proizvodi)
                if (proizvodi.length > 0)
                    mysql.insertUpdatePrices(proizvodi, (data) => {
                        console.log("Updated ceni..");
                        cb({'Proizvodi': data})
                    })
                else
                    console.log("Proizvodi - fetch problem");
        });
        mssql.getProducts(proizvodi => {
            //MySQL Upsert
            // console.log(JSON.stringify(proizvodi), "Qqqqqqqqqqqqqqq");
            if (proizvodi)
                if (proizvodi.length > 0)
                    mysql.insertUpdateProducts(proizvodi, success => {
                        console.log("Updated");
                        cb({'Proizvodi2': success})
                    })
            // console.log(proizvodi);
        })
    } catch (ex) {
        console.log("Error", ex);
    }
};

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}
function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    debug('Listening on ' + bind);
    console.log('Listening on ' + bind)
}
