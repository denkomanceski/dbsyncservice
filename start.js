/**
 * Created by denkomanceski on 8/15/16.
 */
var Service = require('node-windows').Service;
var path = require('path');
// Create a new service object
var svc = new Service({
    name:'oXDatabaseSync',
    description: 'ObjectX Sync Service',
    script: path.join(__dirname, 'main.js')
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
    svc.start();
});

svc.install();